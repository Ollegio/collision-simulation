#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <queue>
#include "event.h"
#include <QElapsedTimer>
#include <QTime>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void paintEvent(QPaintEvent *event);
    void timerEvent(QTimerEvent *event);
    void draw(QPainter *qp);
    void calculateEvents(Particle* particle);

private:
    QVector<Particle*> particles;
    int collisions = 0;
    int timerId;
    bool initialized = false;
    const int ballCount = 10;
    Ui::MainWindow *ui;
    std::priority_queue<Event> eventQueue;
    double currentTime;
    Particle* movedParticle = nullptr;
    QVector2D movedParticleNextPos = QVector2D(0,0), movedParticleLastPos = QVector2D(0,0);
    QElapsedTimer m_time;
    int m_frameCount = 0;
    const int timeStep = 1;
    QVector<QString> debugInfo;

    // QWidget interface
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
};
#endif // MAINWINDOW_H
