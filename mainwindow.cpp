#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QVector2D>
#include <time.h>
#include <iostream>
#include <QMouseEvent>
#include <QTime>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    timerId = startTimer(15);
    m_time.start();
    ui->setupUi(this);
    particles = QVector<Particle*>();
//    for (int i = 0; i < 10; i++) {
//        for (int j = 0; j < 10; j++) {
//            int radius = rand() % 10 + 2;
//            particles.push_back(new Particle(QVector2D(i * 25 + 20, j * 25 + 20), QVector2D(0, 0), radius, radius * radius));
//        }
//    }

//    for (int i = 0; i < 10; i++) {
//        for (int j = 0; j < 1; j++) {
//            int radius = 20;
//            particles.push_back(new Particle(QVector2D(i * 40 + 200, j * 40 + 20), QVector2D(0, 0), radius, radius * radius));
//        }
//    }

    int radius = 20;
//    particles.push_back(new Particle(QVector2D(100, 550), QVector2D(0, 8), radius, radius * radius, 0));
    particles.push_back(new Particle(QVector2D(100, 400), QVector2D(5, 5), radius, radius * radius, 0));
    particles.push_back(new Particle(QVector2D(400, 420), QVector2D(0, 5), radius, radius * radius, 1));
    particles.push_back(new Particle(QVector2D(400, 380), QVector2D(0, 5), radius, radius * radius, 2));

    debugInfo.resize(particles.size());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{

    if (!initialized) {
        currentTime = 0;
        for (auto p : particles) {
            calculateEvents(p);
        }
        initialized = true;
        eventQueue.push(Event(currentTime + timeStep, EVENT_TYPE::REDRAW));
    }
    QPainter qp(this);
    if (m_time.elapsed() >= 1000) {
        m_frameCount = 0;
        m_time.restart();
    }
    qp.drawText(750, 10, QString::number(m_frameCount / float(m_time.elapsed()) * 1000));
    m_frameCount++;
    draw(&qp);
}

void MainWindow::timerEvent(QTimerEvent *e)
{
    if (movedParticle != nullptr) {
        if (!movedParticleNextPos.isNull() && !movedParticleLastPos.isNull()) {
            movedParticle->vel = (movedParticleNextPos - movedParticleLastPos) / 5;
            calculateEvents(movedParticle);
        }
        movedParticleLastPos = movedParticle->pos;
    }

    collisions++;
    repaint();
    while (!eventQueue.empty()) {
        Event event = eventQueue.top();
        if (!event.valid()) {
            eventQueue.pop();
            continue;
        }

        for (auto &p : particles) {
            p->pos += p->vel * (event.getTime() - currentTime);
            p->vel += Particle::getGravity();
        }

        currentTime = event.getTime();

        switch(event.getType()) {
        case EVENT_TYPE::COLLIDE_X:
            event.a->collideX();
            eventQueue.pop();
            calculateEvents(event.a);
            break;
        case EVENT_TYPE::COLLIDE_Y:
            event.a->collideY();
            eventQueue.pop();
            calculateEvents(event.a);
            break;
        case EVENT_TYPE::COLLIDE_PARTICLE:
            event.a->collideParticle(event.b);
            eventQueue.pop();
            calculateEvents(event.a);
            calculateEvents(event.b);
            break;
        case EVENT_TYPE::REDRAW:
            eventQueue.pop();
            eventQueue.push(Event(currentTime + timeStep, EVENT_TYPE::REDRAW));
            return;
            break;
        }
    }
}

void MainWindow::draw(QPainter *qp)
{
    QPen pen(Qt::black, 10, Qt::SolidLine, Qt::RoundCap);

    qp->drawText(10, 10, debugInfo[0]);
    qp->drawText(10, 20, debugInfo[1]);
    qp->drawText(10, 30, debugInfo[2]);
    qp->drawText(10, 40, QString::number(currentTime));
    qp->drawText(10, 50, QString::number(eventQueue.size()));

    for (auto &p : particles) {
        pen.setColor(QColor(p->getId() * 100, 0, 0, 50));
//        pen.setColor(QColor(std::min(p->vel.lengthSquared() * 5, 255.0f), 0, std::max(255 - p->vel.lengthSquared() * 5, 0.0f), 200));
        pen.setWidth(p->getRadius() * 2);
        qp->setPen(pen);
        qp->drawPoint(p->pos.toPointF());
//        p->state = std::max(p->state - 1, 0);
    }
}

void MainWindow::calculateEvents(Particle *particle)
{
    auto vel = particle->vel;
    auto pos = particle->pos;
    float dt;
    auto dr = this->height() - particle->getRadius() - pos.y();
    auto acc = Particle::getGravity().y();
    auto d = vel.y() * vel.y() - 2 * acc * (-dr);

    if (d >= 0) {
        auto dt1 = - (vel.y() + sqrt(d)) / acc;
        auto dt2 = - (vel.y() - sqrt(d)) / acc;

        debugInfo[particle->getId()] = QString::number(dt1) + " " + QString::number(dt2);

//        eventQueue.push(Event(currentTime + std::max(dt1, dt2), EVENT_TYPE::COLLIDE_Y, particle));
//        eventQueue.push(Event(currentTime + std::max(dt1, dt2), EVENT_TYPE::COLLIDE_Y, particle));
//        if (dt1 > __FLT_EPSILON__ ^ dt2 > __FLT_EPSILON__) {
//            eventQueue.push(Event(currentTime + std::max(dt1, dt2), EVENT_TYPE::COLLIDE_Y, particle));
//        }
//        if (dt1 > __FLT_EPSILON__ && dt2 > __FLT_EPSILON__) {
//            eventQueue.push(Event(currentTime + std::min(dt1, dt2), EVENT_TYPE::COLLIDE_Y, particle));
//        } else {
            eventQueue.push(Event(currentTime + std::max(dt1, dt2), EVENT_TYPE::COLLIDE_Y, particle));
//        }
//        if (dt1 > -__FLT_EPSILON__) {
//            if (dt2 > -__FLT_EPSILON__) {
//                eventQueue.push(Event(currentTime + std::max(dt1, dt2), EVENT_TYPE::COLLIDE_Y, particle));
//            } else {
//                eventQueue.push(Event(currentTime + dt1, EVENT_TYPE::COLLIDE_Y, particle));
//            }
//        } else if (dt2 > -__FLT_EPSILON__) {
//            eventQueue.push(Event(currentTime + dt2, EVENT_TYPE::COLLIDE_Y, particle));
//        }
    }

        dr = particle->getRadius() - pos.y();
        d = vel.y() * vel.y() - 2 * acc * (-dr);
        if (d > __FLT_EPSILON__) {
            auto dt1 = - (vel.y() + sqrt(d)) / acc;
            auto dt2 = - (vel.y() - sqrt(d)) / acc;

            if (dt1 > __FLT_EPSILON__ && dt2 > __FLT_EPSILON__) {
                eventQueue.push(Event(currentTime + std::min(dt1, dt2), EVENT_TYPE::COLLIDE_Y, particle));
            }
        }

    if (vel.x() > __FLT_EPSILON__) {
        dt = (this->width() - particle->getRadius() - pos.x()) / vel.x();

        eventQueue.push(Event(currentTime + dt, EVENT_TYPE::COLLIDE_X, particle));
    } else if (vel.x() < -__FLT_EPSILON__) {
        dt = (particle->getRadius() - pos.x()) / vel.x();

        eventQueue.push(Event(currentTime + dt, EVENT_TYPE::COLLIDE_X, particle));
    }

    for (auto p : particles) {
        if (&p == &particle) {
            continue;
        }

        auto dr = p->pos - pos;
        auto dv = p->vel - vel;

        auto vr = QVector2D::dotProduct(dv, dr);
        auto v2 = QVector2D::dotProduct(dv, dv);
        auto r2 = QVector2D::dotProduct(dr, dr);

        if (vr > -__FLT_EPSILON__) {
            continue;
        }
        auto sigma = p->getRadius() + particle->getRadius();
        auto d = vr * vr - v2 * (r2 - sigma * sigma);

        if (d < __FLT_EPSILON__) {
            continue;
        }

        dt = - (vr + sqrt(d)) / v2;

        eventQueue.push(Event(currentTime + dt, EVENT_TYPE::COLLIDE_PARTICLE, particle, p));
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    for (auto &p : particles) {
        if (p->pos.distanceToPoint(QVector2D(event->pos())) <= p->getRadius()) {
            movedParticle = p;
//            p->state = 10;
        }
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (movedParticle == nullptr) {
        return;
    }
    movedParticle->state = PARTICLE_STATE::DRAGGED;
    auto mousePos = QVector2D(event->pos());
    movedParticleNextPos = mousePos;
    movedParticle->collide();
    calculateEvents(movedParticle);
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    if (movedParticle == nullptr) {
        return;
    }
    movedParticle->state = PARTICLE_STATE::NORMAL;

    movedParticle = nullptr;
    movedParticleNextPos = QVector2D();
    movedParticleLastPos = QVector2D();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == 'S') {
        for (auto &p : particles) {
            p->vel.setX(0);
            p->vel.setY(0);
        }
    }
    if (event->key() == 'F' && movedParticle != nullptr) {
        movedParticle->state = PARTICLE_STATE::FREEZED;
    }
    if (event->key() == 'R') {

    }
}

