#ifndef PARTICLE_H
#define PARTICLE_H

#include <QPointF>
#include <QVector2D>

enum PARTICLE_STATE
{
    NORMAL,
    FREEZED,
    DRAGGED,
};

class Particle
{
public:
    Particle(QVector2D pos, QVector2D vel, int radius = 5, int mass = 5, int id = 0);
    QVector2D pos;
    QVector2D vel;
    int getCollisionCount();
    void collide();
    void collideX();
    void collideY();
    void collideParticle(Particle* other);
    int getState();
    int getRadius();
    int state = 0;
    int getMass() const;

    int getId() const;

    static const QVector2D &getGravity();

private:
    int id;
    int collisionCount = 0;
    int radius = 5;
    int mass = 5;

    constexpr static const QVector2D gravity = QVector2D(0, 0.05);
};

#endif // PARTICLE_H
