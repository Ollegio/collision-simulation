#include "event.h"

Event::Event(double t, int type, Particle *a, Particle *b): time(t), a(a), b(b), type(type)
{
    if (a != nullptr) {
        collisionCountA = a->getCollisionCount();
    }
    if (b != nullptr) {
        collisionCountB = b->getCollisionCount();
    }
}

double Event::getTime() const
{
    return this->time;
}

bool Event::valid()
{
    bool result = true;
    if (a != nullptr) {
        result = result && a->getCollisionCount() == collisionCountA;
    }
    if (b != nullptr) {
        result = result && b->getCollisionCount() == collisionCountB;
    }

    return result;
}

bool Event::operator<(const Event &that) const
{
    return this->time > that.time;
}

int Event::getType() const
{
    return type;
}
