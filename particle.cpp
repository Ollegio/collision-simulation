#include "particle.h"

Particle::Particle(QVector2D pos, QVector2D vel, int radius, int mass, int id): pos(pos), vel(vel), radius(radius), mass(mass), id(id)
{
}

int Particle::getCollisionCount()
{
    return collisionCount;
}

void Particle::collide()
{
    collisionCount++;
}

void Particle::collideX()
{
//    state = 25;
    collisionCount++;
    vel.setX(-vel.x());
}

void Particle::collideY()
{
//    state = 25;
    collisionCount++;
    vel.setY(-vel.y());
}

void Particle::collideParticle(Particle *other)
{
//    state = 25;
//    other->state = 25;
    collisionCount++;
    other->collisionCount++;

    auto dr = other->pos - pos;
    auto dv = other->vel - vel;

    auto vr = QVector2D::dotProduct(dv, dr);

    auto j = 2 * mass * other->mass * (vr) / (dr.lengthSquared() * (mass + other->mass));

    vel += dr * j / (mass);
    other->vel -= dr * j / (other->mass);
}

int Particle::getState()
{
    return state;
}

int Particle::getRadius()
{
    return radius;
}

int Particle::getMass() const
{
    return mass;
}

int Particle::getId() const
{
    return id;
}

const QVector2D &Particle::getGravity()
{
    return gravity;
}
