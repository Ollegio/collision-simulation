#ifndef EVENT_H
#define EVENT_H

#include "particle.h"

enum EVENT_TYPE
{
    COLLIDE_X,
    COLLIDE_Y,
    COLLIDE_PARTICLE,
    REDRAW,
    COLLIDE_MAXWELL_DOOR,
};

class Event
{
public:
    Event(double t, int type, Particle *a = nullptr, Particle *b = nullptr);
    Particle *a, *b;
    double getTime() const;
    bool valid();
    bool operator<(const Event& that) const;

    int getType() const;

private:
    double time;
    int collisionCountA, collisionCountB;
    int type;
};

#endif // EVENT_H
